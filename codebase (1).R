library(tidyverse)
library(jsonlite)
library(tibble)
library(stringr)
library(ggmap)
library(forcats)
library(ggplot2)
library(dplyr)



yelp_new <- yelp_tbl %>% select(-starts_with("hours"), - starts_with("attributes")) %>% filter(str_detect(categories, "Restaurant"))
 
yelp_new %>% unnest(categories) %>%  count(state) %>%  arrange(desc(n))

ggmap(get_map("United States of America",zoom=4,color = "bw")) +   
  geom_point(data=yelp_new,
            aes(x=longitude,y=latitude),
            size=3,color='red')  


##-----------------------------------------------------------------------------
## Load & convert "reviews" file
suppressPackageStartupMessages(library('BBmisc'))
suppressAll(library('jsonlite'))
suppressAll(library('plyr'))
suppressAll(library('dplyr'))
suppressAll(library('stringr'))
suppressAll(library('doParallel'))
registerDoParallel(cores=16)

dat <- llply("user.json", function(x) stream_in(file(x),pagesize = 10000),.progress='=')
saveRDS(dat, file = "user.rds")
##-----------------------------------------------------------------------------

## Load data
reviews <- data.frame(readRDS("review.rds"))
business <- data.frame(readRDS("RDSbusiness_table"))

##-----------------------------------------------------------------------------

##Creat RDS file which contains the reviews that only related to "restaurant" category 

business_review <- business %>%
  filter(str_detect(categories, "Restaurant")) %>%
  filter(state == 'AZ') %>%
  select(business_id, name) %>%
  left_join(.,x)

saveRDS(business_review, file = "business_review.rds")

business_review <- data.frame(readRDS("business_review.rds"))


##----------------------------------------------------------------------------
## Text mining
library(tm)
library(tidytext)
library(wordcloud)
library(dplyr)

restaurant <- business_review %>% 
  filter(name == 'Trader Vic\'s') %>% 
  rename(doc_id = review_id)

text.c <- VCorpus(DataframeSource(select(restaurant, doc_id, text)))

meta.data <- restaurant %>% 
  select(doc_id, stars, useful, funny, cool, date) %>%
  rename(documnet = doc_id)

Sys.setlocale(category = "LC_CTYPE", locale = "C")

DTM.res <- DocumentTermMatrix(text.c,
                control=list(removePunctuation=TRUE,
                             wordLengths=c(3, Inf),
                             stopwords=TRUE,
                             stemming=TRUE,
                             removeNumbers=TRUE))

print(DTM.res)

DTM.res.sp <- removeSparseTerms(DTM.res,0.995)

print(DTM.res.sp)

## Most frequent words for one restaurant
library(forcats)
library(ggplot2)

restaurant.tidy <- tidy(DTM.res.sp)

term.count <- restaurant.tidy %>%
  group_by(term) %>%
  summarize(n.total=sum(count)) %>%
  arrange(desc(n.total))


term.count %>% 
  slice(1:30) %>%
  ggplot(aes(x=fct_reorder(term,n.total),y=n.total)) + geom_bar(stat='identity') + 
  coord_flip() + xlab('Counts') + ylab('')+ggtitle('Most Frequent Terms')

term.count.pop <- term.count %>%
  slice(5:100) 

wordcloud(term.count.pop$term, term.count.pop$n.total, scale=c(5,.5))

##--------------------------------------------------------------------
## Average ratings

avg_rating <- business_review %>% 
                  group_by(name) %>% 
                  summarize(avg_star = mean(stars)) %>%
                  arrange(desc(avg_star))

best_rated <- slice(avg_rating, 1:100)

worst_rated <- tail(avg_rating,100)

Sys.setlocale(category = "LC_CTYPE", locale = "C")

## Best rated reviews

best_review <- best_rated %>% 
    left_join(.,business_review)  

## Text mining for best rated restaurants
text_best <- best_review %>%  rename(doc_id = review_id)

text.b <- VCorpus(DataframeSource(select(text_best, doc_id, text)))

meta.data <- text_best %>% select(doc_id, stars, useful, funny, cool, date) %>%
  rename(documnet = doc_id)


DTM.best <- DocumentTermMatrix(text.b,
                control=list(removePunctuation=TRUE,
                             wordLengths=c(4, Inf),
                             stopwords=TRUE,
                             stemming=TRUE,
                             removeNumbers=TRUE))

print(DTM.best)

DTM.best.sp <- removeSparseTerms(DTM.best,0.995)

print(DTM.best.sp)

## Most frequent words for best rated restaurants

best.tidy <- tidy(DTM.best.sp)

best.term.count <- best.tidy %>%
  group_by(term) %>%
  summarize(n.total=sum(count)) %>%
  arrange(desc(n.total))


best.term.count %>% 
  slice(1:30) %>%
  ggplot(aes(x=fct_reorder(term,n.total),y=n.total)) + geom_bar(stat='identity') + 
  coord_flip() + xlab('Counts') + ylab('')+ggtitle('Most Frequent Terms')

best.term.count.pop <- best.term.count %>%
  slice(5:100) 

wordcloud(best.term.count.pop$term, best.term.count.pop$n.total, scale=c(5,.5))



## Worst rated reviews

worst_review <- worst_rated %>% 
  left_join(.,business_review)  

## Text mining for worst rated restaurants
text_worst <- worst_review %>%  rename(doc_id = review_id)

text.w <- VCorpus(DataframeSource(select(text_worst, doc_id, text)))

meta.data <- text_worst %>% select(doc_id, stars, useful, funny, cool, date) %>%
  rename(documnet = doc_id)

DTM.worst <- DocumentTermMatrix(text.w,
                               control=list(removePunctuation=TRUE,
                                            wordLengths=c(4, Inf),
                                            stopwords=TRUE,
                                            stemming=TRUE,
                                            removeNumbers=TRUE
                               ))

print(DTM.worst)

DTM.worst.sp <- removeSparseTerms(DTM.worst,0.995)

print(DTM.worst.sp)

## Most frequent words for worst rated restaurants

worst.tidy <- tidy(DTM.worst.sp)

worst.term.count <- worst.tidy %>%
  group_by(term) %>%
  summarize(n.total=sum(count)) %>%
  arrange(desc(n.total))


worst.term.count %>% 
  slice(1:30) %>%
  ggplot(aes(x=fct_reorder(term,n.total),y=n.total)) + geom_bar(stat='identity') + 
  coord_flip() + xlab('Counts') + ylab('')+ggtitle('Most Frequent Terms')

worst.term.count.pop <- worst.term.count %>%
  slice(5:100) 

wordcloud(worst.term.count.pop$term, worst.term.count.pop$n.total, scale=c(5,.5))

