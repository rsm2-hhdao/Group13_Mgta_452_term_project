suppressPackageStartupMessages(library('BBmisc'))
suppressAll(library('jsonlite'))
suppressAll(library('plyr'))
suppressAll(library('dplyr'))
suppressAll(library('stringr'))
suppressAll(library('doParallel'))
library(caret)
library(tidyverse)
library(plyr)

# loading review.rds from local drive
reviews <- data.frame(readRDS("review.rds")) 

# loading business.json from local drive
business <- as_data_frame(flatten(stream_in(file("business.json"))))

## ignore the below part till the line as i am still working on siltering business file. once it is done, i will merge reviews to it so that 
## it will be small file and easy to handale
business_review <- business %>% 
  left_join(.,reviews, by = 'business_id')  

business_review <- data.frame(readRDS("business_review_all.rds"))

glimpse(business)


##--------------------------------------------------------------------------------


## Cleaning business.json

## Since the data is huge, filtering only businesses with more than 50 reviews 
## and are restaurants

business_1 <- business %>% 
  filter(review_count > 50) %>% filter(str_detect(categories, "Restaurant"))
  
glimpse(business_1)

## getting the total missing values for each variable
colSums(is.na(business_1))
colSums(!is.na(business_1$business_id))


## filtering the variables by keeping only those which have less than 50% NA
## filtering uneeded columns start with "hours"
x <- colnames(business_1)[colSums(is.na(business_1))/sum(!is.na(business_1$business_id)) <= 0.50]

business_2 <- business_1[,x] %>% 
  select(-starts_with("hours")) 

glimpse(reviews)

## converting three tables to factors in order to impute missing values

business_2$attributes.Alcohol <- 
  factor(business_2$attributes.Alcohol, 
         levels = c('NA', 'none', 'beer_and_wine','full_bar')
         )

business_2$attributes.NoiseLevel <- 
  factor(business_2$attributes.NoiseLevel, 
         levels = c('average', 'loud', 'very_loud')
  )

business_2$attributes.WiFi <- 
  factor(business_2$attributes.WiFi, 
         levels = c('no', 'paid', 'free')
  )


## removing categories column as it is not required now
categories <- unique(unlist(business_2$categories))

# removing categories column
business_2$categories <- NULL

## imputing our business_2 dataframe to fix the missing values
to_be_imputed <- data.frame(business_2)

install.packages("mice")
library("mice")

imp_business_2 <- mice(to_be_imputed, m=1, maxit=3, seed=500,nnet.MaxNWts = 2600)

business_complete <- complete(imp_business_2,1)

## checking if any variable as zero or near zero variance (nothing present in this case)
zero_var <- nearZeroVar(business_complete, saveMetrics = TRUE)

##======================
## Regression model
cols <- colnames(business_2[13:35])
f <- as.formula(paste('stars ~ ', paste0(colnames(business_2[13:35]), collapse = '+')))

## work in progress
mylogit <- glm(f, business_2, family = 'binomial')
summary(mylogit)

##================== Hanh model
glimpse(business_2)
business_2$attributes.RestaurantsPriceRange2<- 
  factor(business_2$attributes.RestaurantsPriceRange2) %>% 
  revalue(c("1" = "<$10","2" = "$11-$30" ,"3" = "$31-$60" , "4" = ">$61"))

business_2$review_count <- cut(business_2$review_count, 
                        breaks = c(50,200,500,1000))

##Define success level
data <- business_2 %>% mutate(lsuccess = ifelse(stars >= 4, TRUE, FALSE))

##Select test and train data
train <- sample_n(data,5000)
test <- data %>% filter(!(data$business_id %in% train$business_id)) %>% 
  filter(!state=="HLD")

##Model
model_1 <- glm(lsuccess ~ state + review_count +  attributes.RestaurantsPriceRange2 
             +  attributes.BusinessAcceptsCreditCards + attributes.WiFi + 
               attributes.NoiseLevel + attributes.GoodForKids, data = train,family=binomial(link="logit"))
summary(model_1)

##Result
test$Prob <- predict(model_1, newdata = test, type = "response")

test_result <- test %>% mutate(Success_Pred = Prob > 0.5)
Tab_1 <- table(test_result$lsuccess,test_result$Success_Pred)
accuracy <- (Tab_1[1,1]+Tab_1[2,2])/sum(Tab_1)



test_result %>% select(business_id, Prob) %>% arrange(desc(Prob))

###word cloud of the most success restaurant ==================
business_review <- reviews %>% filter(business_id == 'vL44Ky9VxUtOgteyHasRIA') %>%
  select(review_id,text)  

colnames(business_review)[1] <- 'doc_id'

install.packages(c("tm","wordcloud","tidytext"))  ## only run once
library(tm)
library(tidytext)
library(RColorBrewer)
library(wordcloud)
library(forcats)

text.c <- VCorpus(DataframeSource(select(business_review,doc_id,text)))
DTM.aria <- DocumentTermMatrix(text.c,
                               control=list(removePunctuation=TRUE,
                                            wordLengths=c(3, Inf),
                                            stopwords=TRUE,
                                            stemming=TRUE,
                                            removeNumbers=TRUE
                               ))
DTM.aria.sp <- removeSparseTerms(DTM.aria,0.995)

#conflict of plyr and dplyr
detach("package:plyr", unload=TRUE)

aria.tidy <- tidy(DTM.aria.sp)
aria.tidy$term <- factor(aria.tidy$term )
aria.tidy %>% group_by(term) %>% summarise(n=n())

term.count <- aria.tidy %>%
  group_by(term) %>%
  summarize(n.total=sum(count)) %>%
  arrange(desc(n.total))

term.count.pop <- term.count %>%
  slice(5:100) 

wordcloud(term.count.pop$term, term.count.pop$n.total, scale=c(5,.5))
